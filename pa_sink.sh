#!/bin/bash
print_usage() 
{
    printf "Script that runs pacmd local loopback, allows for streaming audio
            from other devices from this device's speakers:
        -l : set latency (in miliseconds, default is 2 msecs)
        -u : unload loopback pulseaudio module
        -h : print help"
}

unload_sink()
{
    if pacmd list | grep -q module-null-sink; then 
        pacmd unload-module module-null-sink
        echo "Sink unloaded."
    else 
        echo "Module was not loaded before." 
    fi

    if pacmd list | grep -q module-loopback ; then 
        pacmd unload-module module-loopback
        echo "loopback unloaded."
    else 
        echo "Module was not loaded before." 
    fi

}

load_sink()
{
    pacmd load-module module-null-sink sink_name=MySink
    pacmd update-sink-proplist MySink device.description=MySink
    pacmd load-module module-loopback sink=MySink
}

while getopts 'lu' flag; do
case "${flag}" in
    l) 
        load_sink
        exit 1
        ;;
    u)
        unload_sink
        exit 1
        ;;
    ?) 
        print_usage 
        exit 1
        ;;
  esac
done


echo "Sink loaded!"
