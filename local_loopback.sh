#!/bin/bash

p_latency_msec=5;

print_usage() 
{
    printf "Script that runs pacmd local loopback, allows for streaming audio
            from other devices from this device's speakers:
        -l : set latency (in miliseconds, default is 5 msecs)
        -u : unload loopback pulseaudio module
        -h : print help"
}

unload_module()
{
    if pacmd list | grep -q module-loopback ; then 
        pacmd unload-module module-loopback
        echo "Module unloaded."
    else 
        echo "Module was not loaded before." 
    fi
}

while getopts 'l:u' flag; do
case "${flag}" in
    l) 
        p_latency_msec=${OPTARG} 
        ;;
    u)
        unload_module
        exit 1
        ;;
    ?) 
        print_usage 
        exit 1
        ;;
  esac
done

unload_module
pacmd load-module module-loopback latency_msec="${p_latency_msec}"
echo "Module loaded with ${p_latency_msec} msec of latency."
