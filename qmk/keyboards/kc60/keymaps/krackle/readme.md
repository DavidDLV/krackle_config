# The default keymap for KC60

![keymap](https://raw.githubusercontent.com/noroadsleft/qmk_images/master/keyboards/kc60/keymaps/default/keymap.png)

The `Fn` key triggers `KC_APP` when tapped, and activate Layer 1 when held.
