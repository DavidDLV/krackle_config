#include QMK_KEYBOARD_H

#define TAPPING_TERM 175

// LAYERS
enum layer_names {
    _QWERTY = 0,
    _ARROW,
    _FUNC
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_QWERTY] = LAYOUT_60_ansi(
    // Base Layer, Basic QWERTY layout 
    // Caps and Left Ctrl are flipped on the KC60
    //  1        2        3        4        5        6        7        8        9        10       11       12       13             14
        KC_GRAVE,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,        KC_BSPC,
        KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC,       KC_BSLS,
        KC_ESC,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT,
        KC_LSFT,   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,       
        KC_LCTL, KC_LGUI, KC_LALT,                            KC_SPC,                               KC_RALT, TG(_ARROW), LT(_FUNC, KC_APP), KC_RCTL
    ),

    [_ARROW] = LAYOUT_60_ansi(
    //  Layout for arrow keys
    //  1           2        3        4        5        6        7        8        9          10       11       12        13             14
        KC_GRAVE,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,     KC_8,     KC_9,    KC_0,    KC_MINS, KC_EQL,        KC_BSPC,
        KC_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,     KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC,       KC_BSLS,
        KC_ESC,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_LEFT, KC_DOWN,  KC_UP,   KC_RGHT,  KC_QUOT, KC_ENT,
        KC_LSFT,   KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM,  KC_DOT,  KC_SLSH,  KC_RSFT,       
        KC_LCTL, KC_LGUI, KC_LALT,                            KC_SPC,                                KC_RALT,  _______, LT(_FUNC, KC_APP), KC_RCTL
    ),

    [_FUNC] = LAYOUT_60_ansi(
    // Basic layout for special and function keys. 
    //  1         2        3        4        5        6        7        8        9        10       11       12       13       14
         KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL,
        XXXXXXX,  KC_PGDN, KC_UP,   KC_PGUP, KC_HOME, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PSCR, KC_SLCK, KC_PAUS, KC_INS,
        XXXXXXX,  KC_LEFT, KC_DOWN, KC_RGHT,  KC_END, XXXXXXX, XXXXXXX, KC_MPRV, KC_MSTP, KC_MPLY, KC_MNXT, XXXXXXX, XXXXXXX,
        KC_CAPS,  XXXXXXX, BL_DEC,  BL_TOGG, BL_INC,  BL_BRTG, XXXXXXX, KC_MUTE, KC_VOLD, KC_VOLU, XXXXXXX,          KC_CAPS,
        _______,  _______, _______,                            XXXXXXX,                            _______, _______, _______, _______
    )

};
