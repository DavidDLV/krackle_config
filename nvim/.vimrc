" David's vimrc
"
set nocompatible

" settings for neovim
"if has('nvim')
    "set g:loaded_python_provider=1 
    "set g:loaded_python3_provider=1 
"endif 

" Guard ttymouse
"if !has('nvim')
    "set ttymouse=xterm2
"endif 

" Visual Settings
set number		    " show line numbers
set cursorline		" highlight the current line
set wildmenu		" visual autocomplete for command menu
set lazyredraw 		" redraw only when needed

syntax enable		" enable syntax processing
filetype plugin on
filetype indent on	" load filetype-specific indent files
syntax on

if has("gui_running")
    set guifont=xos4\ Terminess\ Powerline\ 12
endif


" max line settings (80)
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" spaces/tabs settings

set tabstop=4       " The width of a TAB is set to 4.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of 4.

set shiftwidth=4    " Indents will have a width of 4

set softtabstop=4   " Sets the number of columns for a TAB

set expandtab       " Expand TABs to spaces
set smarttab

" Show statusline all the time
set laststatus=2

" disable beeping/flashing 
set noeb vb t_vb=

set mouse=a

set shell=/usr/bin/fizsh
 "Automatically save the session when leaving Vim
"autocmd! VimLeave * mksession
 "Automatically load the session when entering vim
"autocmd! VimEnter * source ~/Session.vim

map <Space> <Leader>

" ----------Shortcuts
" fix spelling mistakes
nnoremap <Leader>fs 1z=

" toggle linewrapping 
nnoremap <Leader>wr :set wrap! wrap?<CR>

" open vimrc
nnoremap <Leader>rc :e ~/.vimrc<CR>

" Run the A Command (Open corresponding header/source file) in a new window
nnoremap <Leader>sa :vsplit \| :A<CR>
nnoremap <Leader>aa :A<CR>

nnoremap <Leader>go :Goyo <CR>
nnoremap <Leader>wo :MaximizerToggle<CR>


" Limelight
"nmap <Leader>l <Plug>(Limelight)
"" Color name (:help cterm-colors) or ANSI code
"let g:limelight_conceal_ctermfg = 'gray'
""let g:limelight_conceal_ctermfg = 240

"" Color name (:help gui-colors) or RGB color
"let g:limelight_conceal_guifg = 'DarkGray'
""let g:limelight_conceal_guifg = '#777777'

" Default: 0.5
"let g:limelight_default_coefficient = 0.7

"" Number of preceding/following paragraphs to include (default: 0)
"let g:limelight_paragraph_span = 0

"" Beginning/end of paragraph
""   When there's no empty line between the paragraphs
""   and each paragraph starts with indentation
"let g:limelight_bop = '^\s'
"let g:limelight_eop = '\ze\n^\s'

"" Highlighting priority (default: 10)
""   Set it to -1 not to overrule hlsearch
"let g:limelight_priority = -1

" switch higlight no matter the previous state
"nmap <F4> :set hls! <cr>
" hit '/' highlights then enter search mode
nnoremap / :set hlsearch<cr>/

tnoremap <Esc> <C-\><C-n>

nmap <C-k><C-k> :Relativity!<CR><BAR>:set invnumber<CR>

" CtrlSF
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

" build cmake
"nnoremap <Leader>m :!make
 

" ----------plugin settings
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
" - Run ':PlugInstall' to install plugins

call plug#begin ('~/.vim/custom_plugins')

" Code highlighting and completion
Plug 'Valloric/YouCompleteMe'
"if has('nvim')
  "Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  "Plug 'zchee/deoplete-clang'
"else
  "Plug 'Shougo/deoplete.nvim'
  "Plug 'roxma/nvim-yarp'
  "Plug 'roxma/vim-hug-neovim-rpc'
  "Plug 'zchee/deoplete-clang'
"endif

" Code formatting
Plug 'vim-scripts/a.vim'
"Plug 'Raimondi/delimitMate'
"Plug 'junegunn/limelight.vim'
Plug 'Yggdroot/indentLine'
Plug 'godlygeek/tabular'
Plug 'scrooloose/nerdcommenter'
Plug 'sjl/gundo.vim'
Plug 'vim-scripts/DoxygenToolkit.vim'
Plug 'vim-scripts/c.vim'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'burnettk/vim-angular'
Plug 'leafgarland/typescript-vim'
Plug 'docunext/closetag.vim'


" Color theming
Plug 'flazz/vim-colorschemes'
"Plug 'powerline/powerline', { 'rtp': 'powerline/bindings/vim' }
Plug 'sickill/vim-monokai'
Plug 'chriskempson/base16-vim'
Plug 'morhetz/gruvbox'

" Interface Improvements
Plug 'kshenoy/vim-origami'
Plug 'ericcurtin/CurtineIncSw.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'thaerkh/vim-workspace'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
"Plug 'yuttie/comfortable-motion.vim'
"Plug 'terryma/vim-smooth-scroll'
Plug 'kennykaye/vim-relativity'
Plug 'szw/vim-maximizer'
Plug 'xtal8/traces.vim'
Plug 'junegunn/goyo.vim'
"Plug 'vim-scripts/RltvNmbr.vim'
"Plug 'terryma/vim-multiple-cursors'

" Integration
Plug 'metakirby5/codi.vim'
Plug 'rosenfeld/conque-term'
Plug 'dyng/ctrlsf.vim'
Plug 'tpope/vim-fugitive'
"Plug 'huawenyu/neogdb.vim'
Plug 'mhinz/neovim-remote'
Plug 'sakhnik/nvim-gdb'
Plug 'daeyun/vim-matlab'
Plug 'vim-scripts/MatlabFilesEdition'
Plug 'danro/rename.vim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'severin-lemaignan/vim-minimap'
Plug 'lervag/vimtex'

call plug#end()

"----------Other plugins
"set runtimepath=~/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,~/.vim/after

set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"  "for vim-latexsuite

"----------MATLAB Plugins


" ----------plugin configuration
set showcmd	    	" shows last command in bottom bar

" colorscheme plugin
"colorscheme base16-default-dark

" Base 16 colors
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

hi Normal ctermbg=none
hi NonText ctermbg=none

" vim-relativity
map <silent> <C-l> :Relativity<CR>

" Nerd Tree 
" Shortcut to open nerd tree
map <silent> <C-n> :NERDTreeToggle<CR>
" change Nerd Tree change directory mode to allow for NERD directory to
" correspond to global vim directory changes
let g:NERDTreeChDirMode = 2

" YouCompleteMe 
let g:ycm_global_ycm_extra_conf = '/home/krackle/.ycm_extra_conf.py'
"let g:ycm_allow_changing_updatetime = 0
" Changing highlighting color rules
"highlight YcmErrorSign guibg=#e00b0b
"highlight YcmErrorLine guibg=#e00b0b
"highlight YcmErrorSession guibg=#e00b0b
"highlight YcmWarningSign guibg=#e0e00b
"highlight YcmWarningLine guibg=#e0e00b
"highlight YcmWarningSection guibg=#e0e00b
"highlight YcmErrorSign guibg=#ff0000
"highlight YcmErrorLine guibg=#ff0000
"highlight YcmErrorSession guibg=#ff0000
"highlight YcmWarningSign guibg=#ffff00
"highlight YcmWarningLine guibg=#ffff00
"highlight YcmWarningSection guibg=#ffff00

"let g:ycm_autoclose_preview_window_after_completion = false;
let g:ycm_add_preview_to_completeopt = 1

" Deoplete
"let g:deoplete#enable_at_startup = 1

" vim-airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" minimap
let g:minimap_highlight='Visual'

" Doxygen
let g:DoxygenToolkit_briefTag_pre="@Synopsis  "
let g:DoxygenToolkit_paramTag_pre="@Param "
let g:DoxygenToolkit_returnTag="@Returns   "
let g:DoxygenToolkit_blockHeader="-------------------------------"
let g:DoxygenToolkit_blockFooter="---------------------------------"
let g:DoxygenToolkit_authorName="David De La Vega"
let g:DoxygenToolkit_licenseTag="" 

" markdown viewer
let vim_markdown_preview_browser='firefox'

" nvim-gdb settings
"let g:nvimgdb_disable_start_keymaps = 1
"map <Leader>dd :GdbStart env SHELL=/usr/bin/bash gdb -q 

" vimtex
let g:vimtex_view_general_viewer = 'mupdf'
